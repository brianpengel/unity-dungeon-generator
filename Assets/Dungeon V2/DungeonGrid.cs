using System.Collections.Generic ;
using UnityEngine ;
using System ;

using RoomType = Room.RoomType ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute]
public class DungeonGrid : ScriptableObject, ISerializationCallbackReceiver {

  [SerializeField] private int width, height ;
  [SerializeField] private List<Room> rooms ;
  [SerializeField] private int idCount ;

  // Deserialization targets
  private Dictionary<int, Room> activeRooms ;
  private Dictionary<int, int> usedTiles ;

  private Func<Room, RoomFacade> facadeFactory ;


  public int Height { get { return height ; } }
  public int Width  { get { return width ; } }

  public Vector2Int Right  { get { return new Vector2Int(1, 0) ; } }
  public Vector2Int Up     { get { return new Vector2Int(0, 1) ; } }


  public RoomFacade CreateRoom (int pos, int width, int height, RoomType type) {
    int id = NextID() ;
    var room = new Room(id, pos, width, height, type) ;

    if(!ContainsRoom(id) && inBounds(room) && !isOccupied(room)) {
      activeRooms.Add(id, room) ;
      var facade = facadeFactory (room) ;
      RegisterTiles(facade) ;

      return facade ;
    }

    return null ;
  }


  public RoomFacade GetRoom (int id) {
    return ContainsRoom(id) ? facadeFactory(activeRooms[id]) : null ;
  }


  public bool RemoveRoom (int id) {
    if(ContainsRoom(id)) {
      ClearRegisteredTiles(facadeFactory(activeRooms[id])) ;
      return activeRooms.Remove(id) ;
    }

    return true ;
  }

  public bool ContainsRoom (int id) {
    return activeRooms.ContainsKey(id) ;
  }

  public void Clear () {
    activeRooms = new Dictionary<int, Room> () ;
    usedTiles = new Dictionary<int, int> () ;
    idCount = 0 ;
  }


  //
  private bool inBounds (Room r) {
    return inBounds(IndexToPosition (r.Position)) &&
           inBounds(RoomEndPosition(r)) ;
  }

  public bool inBounds (Vector2Int pos) {
    return pos.x >= 0 && pos.x < width &&
           pos.y >= 0 && pos.y < height ;
  }


  //
  private bool isOccupied (Room r) {
    foreach(var pair in activeRooms)
      if(RoomsOverlap(r, pair.Value))
        return true ;
    return false ;
  }

  private bool RoomsOverlap (Room a, Room b) {
    return GetRoomRect(a).Overlaps(GetRoomRect(b)) ;
  }

  private Rect GetRoomRect(Room r) {
    var pos = IndexToPosition (r.Position) ;
    return new Rect (pos.x, pos.y, r.Width, r.Height) ;
  }


  //
  private Vector2Int RoomEndPosition (Room r) {
    return IndexToPosition(r.Position) + new Vector2Int(r.Width, r.Height) ;
  }


  //
  public Vector2Int IndexToPosition (int i) {
    if(i == 0) return Vector2Int.zero ;

    int x = i % width,
        y = (i - x) / width ;
    return new Vector2Int(x, y) ;
  }

  public int PositionToIndex (Vector2Int pos) {
    if(!inBounds(pos)) return -1 ;
    return pos.x + pos.y * width ;
  }


  //
  public Dictionary<int, int> GetUsedTiles () {
    return new Dictionary<int, int> (usedTiles) ;
  }

  public int GetUsedTile (int i) {
    if(usedTiles.ContainsKey(i))
      return usedTiles[i] ;
    return -1 ;
  }

  public bool isTileUsed (int i) {
    return usedTiles.ContainsKey(i) ;
  }

  public IEnumerable<int> TileDirectNeighbours (int i, int offset = 1) {
    var pos = IndexToPosition(i) ;
    yield return PositionToIndex (pos + new Vector2Int ( offset,  0)) ;
    yield return PositionToIndex (pos + new Vector2Int (-offset,  0)) ;
    yield return PositionToIndex (pos + new Vector2Int ( 0,  offset)) ;
    yield return PositionToIndex (pos + new Vector2Int ( 0, -offset)) ;
  }

  public IEnumerable<int> TileCardinalNeighbours (int i, int offset = 1) {
    var pos = IndexToPosition(i) ;
    yield return PositionToIndex (pos + new Vector2Int ( offset,  offset)) ;
    yield return PositionToIndex (pos + new Vector2Int (-offset,  offset)) ;
    yield return PositionToIndex (pos + new Vector2Int ( offset, -offset)) ;
    yield return PositionToIndex (pos + new Vector2Int (-offset, -offset)) ;

    yield return PositionToIndex (pos + new Vector2Int ( offset,  0)) ;
    yield return PositionToIndex (pos + new Vector2Int (-offset,  0)) ;
    yield return PositionToIndex (pos + new Vector2Int ( 0,  offset)) ;
    yield return PositionToIndex (pos + new Vector2Int ( 0, -offset)) ;
  }

  public IEnumerable<int> Indices () {
    for (int cnt = 0 ; cnt < width * height ; cnt ++)
      yield return cnt ;
  }

  public IEnumerable<RoomFacade> Rooms () {
    foreach (var room in activeRooms)
      yield return facadeFactory(room.Value) ;
  }

  public IEnumerable<RoomFacade> RoomsOfType (RoomType t) {
    foreach (var room in activeRooms)
      if(room.Value.Type == t)
        yield return facadeFactory(room.Value) ;
  }

  public IEnumerable<int> RoomIndices (RoomFacade room) {
    for (int cnt = 0 ; cnt < room.Width * room.Height ; cnt ++) {
      int x = cnt % room.Width,
          y = (cnt - x) /room. Width ;

      yield return room.Position + x + y * width ;
    }
  }


  //
  private void RegisterTiles (RoomFacade room) {
    foreach(int i in RoomIndices(room))
      usedTiles.Add(i, room.ID) ;
  }

  private void ClearRegisteredTiles (RoomFacade room) {
    foreach(int i in RoomIndices(room))
      usedTiles.Remove(i) ;
  }

  //
  private int NextID () {
    idCount ++ ;
    return idCount ;
  }


  // Serialization
  public void OnBeforeSerialize () {
    if(activeRooms == null) return ;
    rooms = new List<Room> () ;

    foreach(var pair in activeRooms)
      rooms.Add(pair.Value) ;
  }

  public void OnAfterDeserialize () {
    if(rooms == null) return ;
    facadeFactory = RoomFacade.CreateFactory (this, inBounds, isOccupied, RegisterTiles, ClearRegisteredTiles) ;
    Clear () ;

    // Will clear the grid if the ids overlap
    try {
      foreach (var room in rooms) {
        activeRooms.Add(room.ID, room) ;
        RegisterTiles (facadeFactory(room)) ;
      }
    }

    catch(Exception e) {
      Clear () ;
    }
  }
}
