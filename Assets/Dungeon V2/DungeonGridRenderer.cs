using System.Collections.Generic ;
using UnityEngine ;

using RoomType = Room.RoomType ;

/*
 *
 *
 */

public class DungeonGridRenderer : MonoBehaviour {

  public static readonly Quaternion POSITION_ROTATION = Quaternion.Euler (90, 0, 0) ;

  [SerializeField] private DungeonTileInstance prefab ;
  [SerializeField] private TextMesh textPrefab ;
  private Dictionary<DungeonTileInstance, int> rooms ;
  private Dictionary<int, TextMesh> textMeshes ;

  public void Render (DungeonGrid grid) {
    textMeshes = new Dictionary<int, TextMesh> () ;
    rooms = new  Dictionary<DungeonTileInstance, int> () ;
    var offset = Vector3.one * .5f ;

    foreach (var i in grid.GetUsedTiles()) {
      var room = grid.GetRoom (i.Value) ;

      var pos = POSITION_ROTATION * (Vector3)(Vector2) grid.IndexToPosition(i.Key) - offset ;
      var instance = Instantiate(prefab, pos, Quaternion.identity) as DungeonTileInstance ;
      instance.Init (i.Key, grid, GetColour(room.ID, room.Type)) ;
      instance.transform.SetParent (transform) ;

      if(room.Type == RoomType.DEFAULT) {
        rooms.Add(instance, i.Value) ;

        if(!textMeshes.ContainsKey(room.ID)) {
          var textPos = POSITION_ROTATION * (Vector3)(grid.IndexToPosition(room.Position) + new Vector2(room.Width, room.Height) * .5f) - offset ;
          var mesh = Instantiate(textPrefab, textPos, POSITION_ROTATION) as TextMesh ;
          mesh.transform.SetParent (transform) ;
          textMeshes.Add (room.ID, mesh) ;
        }
      }
    }
  }

  public void SetRoomColour (int i, Color colour) {
    foreach (var instance in rooms)
      if(instance.Value == i)
        instance.Key.SetColour (colour) ;
  }

  public void SetRoomText (int i, string text) {
    if(textMeshes.ContainsKey(i))
      textMeshes[i].text = text ;
  }

  private Color GetColour (int seed, RoomType type) {
    switch(type) {
      case RoomType.CONNECTOR:
      return Color.black ;
      case RoomType.CORRIDOR:
      return Color.white ;
      default:
      return ExtensionMethods.GetSeededColour(Color.white, seed) ;
    }
  }

}
