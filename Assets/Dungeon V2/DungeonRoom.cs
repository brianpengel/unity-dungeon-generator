using System.Collections.Generic ;
using UnityEngine ;
using System ;


//
[System.Serializable]
public class Room {

  public enum RoomType {
    DEFAULT, CORRIDOR, CONNECTOR
  }

  public int Width, Height, Position, ID ;
  public RoomType Type ;

  public Room (int id, int pos, int width, int height, RoomType type = RoomType.DEFAULT) {
    this.Position = pos ;
    this.Height   = height ;
    this.Width    = width ;
    this.Type     = type ;
    this.ID       = id ;
  }

}


// Used as a managed substitute for a Room
// ! Wilde wat nieuws proberen !
public class RoomFacade {

  // Delegates used to determine if altering the room is allowed
  private System.Action<RoomFacade> applyTiles, clearTiles ;
  private Func<Room, bool> inBounds, isOccupied ;
  private Room GoverningRoom ;

  public int Position        { get { return GoverningRoom.Position ; } }
  public int Height          { get { return GoverningRoom.Height   ; } }
  public int Width           { get { return GoverningRoom.Width    ; } }
  public int ID              { get { return GoverningRoom.ID       ; } }

  public Room.RoomType Type  { get { return GoverningRoom.Type  ; }
                               set { GoverningRoom.Type = value ; } }


  public RoomFacade (DungeonGrid grid, Func<Room, bool> inBounds, Func<Room, bool> isOccupied, System.Action<RoomFacade> applyTiles, System.Action<RoomFacade> clearTiles, Room room) {
    this.GoverningRoom = room ;
    this.applyTiles    = applyTiles ;
    this.clearTiles    = clearTiles ;
    this.isOccupied    = isOccupied ;
    this.inBounds      = inBounds ;
  }

  public void ChangePosition (int p) {
    if(p >= 0 && p != Position) {
      var testRoom = new Room (-1, p, Width, Height, Type) ;
      if(inBounds(testRoom) && !isOccupied(testRoom))
        GoverningRoom.Position = p ;
    }
  }

  public void ChangeWidth (int w) {
    if(w > Width) {
      var testRoom = new Room (-1, Position, w, Height, Type) ;
      if(inBounds(testRoom) && !isOccupied(testRoom))
        GoverningRoom.Width = w ;
    }

    else if (w > 0 && w != Width)
      // If the width is smaller then the current width of the room it's impossible to intersect with other rooms
      GoverningRoom.Width = w ;
  }

  public void ChangeHeight (int h) {
    if(h > Height) {
      var testRoom = new Room (-1, Position, Width, h, Type) ;
      if(inBounds(testRoom) && !isOccupied(testRoom))
        GoverningRoom.Height = h ;
    }

    else if (h > 0 && h != Height)
      GoverningRoom.Height = h ;
  }


  // FACTORY
  public static Func<Room, RoomFacade> CreateFactory (DungeonGrid grid, Func<Room, bool> inBounds, Func<Room, bool> isOccupied, System.Action<RoomFacade> applyTiles, System.Action<RoomFacade> clearTiles) {
    return (room) => {
      return new RoomFacade (grid, inBounds, isOccupied, applyTiles, clearTiles, room) ;
    } ;
  }

}
