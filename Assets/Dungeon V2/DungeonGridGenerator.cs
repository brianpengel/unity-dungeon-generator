using System.Collections.Generic ;
using System.Collections ;
using System ;
using UnityEngine ;

using RoomType = Room.RoomType ;

/*
 *
 *
 */

public class DungeonGridGenerator : MonoBehaviour {

  [SerializeField] private int RoomPlacementAttempts = 40 ;
  [SerializeField] private int minRoomSize = 3, maxRoomSize = 20 ;


  public void Generate (DungeonGrid grid) {
    grid.Clear () ;

    GenerateRooms (grid) ;
    GenerateCorridors (grid) ;
    RemoveDeadEnds (grid) ;
  }

  private void GenerateRooms (DungeonGrid grid) {
    for(int cnt = 0; cnt < RoomPlacementAttempts; cnt ++) {
      int w = ExtensionMethods.OddRange(minRoomSize, maxRoomSize),
          h = ExtensionMethods.OddRange(minRoomSize, maxRoomSize) ;

      Vector2Int p = new Vector2Int(
        ExtensionMethods.OddRange(1, grid.Width  - w - 1),
        ExtensionMethods.OddRange(1, grid.Height - h - 1)
      );

      grid.CreateRoom( grid.PositionToIndex(p), w, h, RoomType.DEFAULT ) ;
    }
  }


  // INCOMMING UGLY CODE
  private void GenerateCorridors (DungeonGrid grid) {
    var transformedIDs = new Dictionary<int, int> () ;

    // Helpers
    System.Func<int, int> getID = (id) => {
      if(!transformedIDs.ContainsKey(id))
        transformedIDs.Add(id, id) ;
      return transformedIDs[id] ;
    } ;

    // Placement of junctions
    int corridorGroup = -1 ;
    foreach(var index in grid.Indices()) {
      var pos = grid.IndexToPosition(index) ;

      if(pos.x % 2 != 0 && pos.y % 2 != 0 && isElligibleCell(grid, index)) {
        Flood (grid, index, (i, room) => {
          transformedIDs.Add(room.ID, corridorGroup) ;
        }) ;

        corridorGroup -- ;
      }
    }

    var tiles = grid.GetUsedTiles() ;
    var indices = new List<int> (tiles.Keys) ;
    indices = ExtensionMethods.Shuffle<int> (indices) ;

    // Connect junctions with other junctions and rooms
    foreach (var j in indices) {
      // int tileID = pair.Key, roomID = pair.Value ;
      int tileID = j, roomID = tiles[j] ;
      var pos = grid.IndexToPosition(tileID) ;


      foreach (int neighbour in grid.TileDirectNeighbours(tileID)) {
        int neighbourID = grid.GetUsedTile(neighbour) ;
        if(neighbourID != -1) continue ;

        int diff = neighbour - tileID ;
        bool vert = Mathf.Abs(diff) > 1 ;

        var neighbourPos = pos + ((vert ? grid.Up : grid.Right) * 2 * (int)Mathf.Sign(diff)) ;
        int farNeighbour = grid.PositionToIndex(neighbourPos) ;
        if(farNeighbour == -1) continue ;

        int farNeighbourID = grid.GetUsedTile(farNeighbour) ;
        if(farNeighbourID == -1) continue ;

        int fnTransformed = getID (farNeighbourID) ;
        int transformedID = getID (roomID) ;

        if(transformedID != fnTransformed && fnTransformed != roomID && farNeighbourID != roomID && farNeighbourID != transformedID) {
          var connector = grid.CreateRoom( neighbour, 1, 1, RoomType.CONNECTOR ) ;
          transformedIDs.Add(connector.ID, transformedID) ;

          // Update earlier converted tiles
          foreach(int key in (new List<int>(transformedIDs.Keys)))
            if(transformedIDs[key] == fnTransformed)
              transformedIDs[key] = transformedID ;

          break ;
        }
      }
    }
  }

  // Recursive method to place corridors
  private bool Flood (DungeonGrid grid, int i, System.Action<int, RoomFacade> createCorridor) {
    var corridor = grid.CreateRoom( i, 1, 1, RoomType.CORRIDOR ) ;
    if(corridor == null) return false ;
    createCorridor ( i, corridor ) ;

    foreach (int index in grid.TileDirectNeighbours(i)) {
      int farNeighbour = i + (index - i) * 2 ;
      if(Flood (grid, farNeighbour, createCorridor))
        createCorridor ( index, grid.CreateRoom( index, 1, 1, RoomType.CORRIDOR )) ;
    }

    return true ;
  }

  private void RemoveDeadEnds (DungeonGrid grid) {
    var usedTiles = grid.GetUsedTiles () ;

    foreach (var pair in usedTiles) {
      int neighbourCount = 0 ;
      foreach(var i in grid.TileDirectNeighbours(pair.Key))
        neighbourCount += (i != -1  && usedTiles.ContainsKey(i)) ? 1 : 0 ;

      if(neighbourCount == 1) {
        grid.RemoveRoom(pair.Value) ;
        RemoveDeadEnds (grid) ;
        break ;
      }
    }
  }


  private bool isElligibleCell (DungeonGrid grid, int i) {
    var usedTiles = grid.GetUsedTiles () ;

    foreach (int index in grid.TileCardinalNeighbours(i))
      if(index != -1 && usedTiles.ContainsKey(index))
        return false ;
    return !usedTiles.ContainsKey(i) ;
  }

}
