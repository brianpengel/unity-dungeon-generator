﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter), typeof(MeshCollider))]
public class DungeonTileInstance : MonoBehaviour {

		private MeshCollider mCollider ;
	  private MeshFilter mFilter ;
	  private Vector3[] vertices ;
	  private Material material ;

		public int index { get; private set; }

    void Awake() {
	    mCollider = GetComponent<MeshCollider> () ;
	    material = GetComponent<MeshRenderer>().material = new Material(Shader.Find("Diffuse"));
	    mFilter = GetComponent<MeshFilter> () ;
			SetVertices () ;
	  }

	  public void Init (int index, DungeonGrid grid, Color colour) {
			this.material.color = colour ;
			this.index = index ;
	    UpdateMesh(index, grid) ;
	  }

		 public void SetColour (Color colour) {
 			this.material.color = colour ;
 	  }


	  public void UpdateMesh (int index, DungeonGrid grid) {
	    var mesh = mFilter.mesh = mCollider.sharedMesh = new Mesh() ;
	    var builder = new QuadCollection() ;
	    mesh.name = "Generated Tile" ;
	    builder.Add(0, 1, 2, 3) ;

			var neighbours = grid.TileDirectNeighbours(index).GetEnumerator() ;

	    // Checks
			neighbours.MoveNext () ;
			bool left  = grid.isTileUsed(neighbours.Current) ;
			neighbours.MoveNext () ;
			bool right = grid.isTileUsed(neighbours.Current) ;
			neighbours.MoveNext () ;
			bool down  = grid.isTileUsed(neighbours.Current) ;
			neighbours.MoveNext () ;
			bool up    = grid.isTileUsed(neighbours.Current) ;

	    if(!up)   {   builder.Add(28, 30, 29, 31) ; // Top Wall
	      if(left)    builder.Add(8, 9, 10, 11) ;
	      if(right)   builder.Add(16, 17, 18, 19) ;
	    }

	    if(!down) {   builder.Add(32, 33, 34, 35) ; // Down Wall
	      if(left)    builder.Add(4, 5, 6, 7) ;
	      if(right)   builder.Add(12, 13, 14, 15) ;
	    }

	    if(!left) {   builder.Add(4, 9, 6, 11) ;    // Left Wall
	      if(up)      builder.Add(28, 20, 29, 21) ;
	      if(down)    builder.Add(26, 33, 27, 35) ;
	    }

	    if(!right){   builder.Add(16, 13, 18, 15) ;   // Right Wall
	      if(up)      builder.Add(22, 30, 23, 31) ;
	      if(down)    builder.Add(32, 24, 34, 25) ;
	    }

	    mesh.vertices = vertices ;
	    mesh.triangles = builder.Build() ;
	    mesh.RecalculateNormals();
	  }


		public void SetVertices () {
			Vector3 upWall    = Vector3.forward - Vector3.forward,
							downWall  = Vector3.forward,
 	            leftWall  = Vector3.right,
							rightWall = Vector3.right - Vector3.right;

 	    vertices = new Vector3[36] {
 	      // Floor
 	      Vector3.zero, Vector3.right, Vector3.forward, Vector3.forward + Vector3.right,

 	      //Left Connection
 	      downWall + leftWall, downWall, downWall + leftWall + Vector3.up, downWall + Vector3.up,
 	      upWall, upWall + leftWall, upWall + Vector3.up, upWall + leftWall + Vector3.up,

 	      //Right Connection
 	      downWall + Vector3.right, downWall + rightWall, downWall + Vector3.up + Vector3.right, downWall + rightWall + Vector3.up,
 	      upWall + rightWall, upWall + Vector3.right, upWall + rightWall + Vector3.up, upWall + Vector3.right + Vector3.up,

 	      // Up connection
 	      Vector3.forward + leftWall, Vector3.up + Vector3.forward + leftWall,
 	      Vector3.forward + rightWall, Vector3.up + Vector3.forward + rightWall,

 	      // Down connection
 	      rightWall, Vector3.up + rightWall, leftWall, Vector3.up + leftWall,

 	      Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero,
 	      Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero,
 	    } ;

 	    // Duplicate vertices to prevent soft edges
 	    vertices[28] = vertices[9] ;
 	    vertices[29] = vertices[11] ;
 	    vertices[30] = vertices[16] ;
 	    vertices[31] = vertices[18] ;

 	    vertices[32] = vertices[13] ;
 	    vertices[33] = vertices[4] ;
 	    vertices[34] = vertices[15] ;
 	    vertices[35] = vertices[6] ;
	  }
	}


	public class QuadCollection : MeshBuilder {

		public override int[] Build () {
				int vertCount = vertices.Count,
	          quadCount = vertCount / 4 ;
	  		int[] tris = new int[(int)vertCount * 3] ;

	  		int t = 0 ;
	  		for (int cnt = 0, v = 0; cnt < quadCount; cnt++, v += 4)
	  				t = CreateQuad (ref tris, t, vertices [v], vertices [v + 1], vertices [v + 2], vertices [v + 3]);

			return tris ;
		}
	}
