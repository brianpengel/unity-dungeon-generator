using System.Collections.Generic ;

[System.Serializable]
public class RoomData {

  [UnityEngine.SerializeField] int occupationAmount = 0, value, occupyingPlayer = -1 ;
  [UnityEngine.SerializeField] RoomFacade room ;

  public Dictionary<int, int> Connectors { get ; private set ; }
  public int OccupyingPlayer             { get { return occupyingPlayer ;  } set { occupyingPlayer = value ; } }
  public int Occupation                  { get { return occupationAmount ; } set { occupationAmount = value ; } }
  public int Value                       { get { return value ; } }
  public RoomFacade Room                 { get { return room ; } }

  public RoomData (RoomFacade room) {
    this.Connectors = new Dictionary <int, int> () ;
    this.value = room.Width * room.Height ;
    this.room = room ;
  }

}
