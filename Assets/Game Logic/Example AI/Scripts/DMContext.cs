using System.Collections.Generic ;
using System.Collections ;
using System ;

using UnityEngine ;

/*
 * DMContext (Dungeon Master Context)
 * Originally for different AI
 */

public class DMContext : PlayerController, ILoggingContext {

  const float RISK_MULTIPLIER = .1f ;

  [SerializeField] private Intelligence turnAI ;

  private Dictionary<int, RoomData> roomInstances ;
  private Dictionary<int, float> rankedRooms ;
  private Vector2Int[] rankedBattles ;
  private Vector2Int[] rankedMoves ;

  private delegate bool RoomComparer (int a, int b, out float res);
  private Comparison<int> rankComparer ;

  private int[] rankedOrder ;

  public RoomData MostRiskyEnemyRoom  { get ; private set ; }
  public RoomData MostRiskyAllyRoom   { get ; private set ; }

  public bool SituationChanged        { get ; private set ; }
  public bool CanAttack               { get ; private set ; }
  public bool CanMove                 { get { return MovesLeft > 0 && rankedMoves != null && rankedMoves.Length > 0 ; } }

  public ActionLog Log { get ; private set ; }

  void OnEnable () {
    Log = new ActionLog (10) ;
  }

  public override void OnGameStart () {
    base.OnGameStart () ;
    roomInstances = RetrieveRelevantRooms () ;
    rankComparer = new Comparison<int> (
      (a, b) => -rankedRooms[a].CompareTo(rankedRooms[b])
    );
  }

  public override void OnBattle (bool isAttacker, RoomData origin, RoomData battleField, System.Action<int> roll, System.Action endBattle) {
    roll (10) ;
  }

	public override void OnTurnStart () {
    base.OnTurnStart () ;
    SituationChanged = true ;
    CanAttack = true ;
    OnActionEnd () ;
  }

  private void OnActionEnd () {
    StartCoroutine (DelayedActionEnd()) ;
  }

  private IEnumerator DelayedActionEnd () {
    yield return new WaitForSeconds (0f) ;
    var action = turnAI.Act <DMContext> (this) ;

    if(action == null)
         EndTurn () ;
    else Log.LogAction (action) ;
  }

	public override void OnTurnEnd () {

  }

  public override void OnBattled (bool won) {
    OnActionEnd () ;
  }

  public override void OnMove (int amount) {
    base.OnMove (amount) ;
    OnActionEnd () ;
  }

  public override void OnReinforce (int amount) {
    base.OnReinforce (amount) ;
    OnActionEnd () ;
  }


  // Actions
  public void DoInvestigate () {
    SituationChanged = false ;

    // Rank all rooms
    rankedRooms = RankRooms () ;

    // Sort array on room risk value
    rankedOrder = new int[rankedRooms.Count] ;
    rankedRooms.Keys.CopyTo (rankedOrder, 0) ;
    Array.Sort(rankedOrder, rankComparer) ;

    // Store rooms that are most at risk
    MostRiskyEnemyRoom = roomInstances [Array.Find (rankedOrder, e => !isAlly(e))];
    MostRiskyAllyRoom  = roomInstances [Array.Find (rankedOrder, e =>  isAlly(e))];


    rankedBattles = RankBattles () ;
    rankedMoves = RankMoves () ;
    CanAttack = rankedBattles.Length == 0 ? false : CanAttack ;

    OnActionEnd () ;
  }

  public void DoMove () {
    SituationChanged = true ;
    CanAttack = false ;

    base.DoMove(rankedMoves[0].x, rankedMoves[0].y, 1) ;
  }

  public void DoAttack () {
    SituationChanged = true ;
    HasAttacked = true ;

    base.DoAttack (rankedBattles[0].x, rankedBattles[0].y) ;
  }

  public void DoReinforce () {
    SituationChanged = true ;
    CanAttack = false ;

    HasReinforced = true ;
    base.DoReinforce(MostRiskyAllyRoom.Room.ID, 1) ;
  }


  public void SayMessage (string msg) {
    // chat.SendMessage("AI", msg, Chat.MessageType.PLAYER_MESSAGE) ;
  }

  public override void GiveRoom (int i) {
		base.GiveRoom (i) ;

    if(gameStarted)
      roomInstances = RetrieveRelevantRooms () ;
	}

	public override void TakeRoom (int i) {
    base.TakeRoom (i) ;

    if(gameStarted)
      roomInstances = RetrieveRelevantRooms () ;
	}

  private Dictionary<int, RoomData> RetrieveRelevantRooms () {
    var res = new Dictionary<int, RoomData> () ;

    foreach (var room in rooms) {
      if(!res.ContainsKey(room.Key))
        res.Add(room.Key, roomManager.GetRoom(room.Key)) ;
      foreach (var neighbour in room.Value)
        if(!res.ContainsKey(neighbour))
          res.Add(neighbour, roomManager.GetRoom(neighbour)) ;
    }

    return res ;
  }


  // Ranking
  private Dictionary<int, float> RankRooms () {
    var res = new Dictionary<int, float>  () ;

    foreach (var room in roomInstances) {
      var risk = RankRoom(room.Value) ;
      res.Add(room.Key, risk) ;
    }

    return res ;
  }

  private float RankRoom (RoomData room) {
    return RankRoom(room.Room.ID, room.Occupation, room.OccupyingPlayer) ;
  }

  private float RankRoom (int i, int occupation, int playerID) {
    float risk = 0, enemies = 0;

    foreach (var neighbourID in roomManager.GetAdjacent(i))
      if(roomInstances.ContainsKey(neighbourID)) {
        var neighbour = roomInstances[neighbourID] ;

        if(!isAlly(neighbour.OccupyingPlayer)) {
          int diff = neighbour.Occupation - occupation ;
          risk += (diff + 1) * RISK_MULTIPLIER ;
          enemies ++ ;
        }
      }

    return enemies == 0 ? -Mathf.Infinity : risk ;
  }

  public float GetRoomRisk (RoomData r) {
    int i = r.Room.ID ;
    if(rankedRooms != null && rankedRooms.ContainsKey(i))
      return rankedRooms[i] ;
    return 0 ;
  }

  private Vector2Int[] RankMoves () {
    return RankAction (
      delegate (int a, int b, out float c) {
        var neighbour = roomInstances[b] ;
        var og = roomInstances[a] ;
            c = RankRoom(b, neighbour.Occupation + 1, neighbour.OccupyingPlayer) ;
        var d = RankRoom(a, og.Occupation - 1,        og.OccupyingPlayer) ;
        return rankedRooms[a] < rankedRooms[b] && (c + d < rankedRooms[b] + rankedRooms[a]) ;
      },

      true
    ) ;
  }

  private Vector2Int[] RankBattles () {
    return RankAction (
      delegate (int a, int b, out float c) {
        var neighbour = roomInstances[b] ;
        var og = roomInstances[a] ;
            c = rankedRooms[a] - rankedRooms[b] ;
        return c < 1 && neighbour.Occupation - og.Occupation < 1 ;
      }
    ) ;
  }

  private Vector2Int[] RankAction (RoomComparer comparer, bool targetAllies = false) {
    var estimations = new Dictionary<int, float> () ;
    var res = new Dictionary<int, int> () ;

    foreach (var currentRoom in rooms) {
      var roomID = currentRoom.Key ;

      // Foreach neighbouring room
      foreach (var neighbourID in currentRoom.Value) {
        float estimation ;

        if(!isAlly (neighbourID) ^ targetAllies && comparer(roomID, neighbourID, out estimation))
          if(!estimations.ContainsKey(neighbourID)) {
            estimations.Add(neighbourID, estimation) ;
            res.Add(neighbourID, roomID) ;
          }

          else if(estimations[neighbourID] >= estimation) {
            estimations[neighbourID] = estimation ;
            res[neighbourID] = roomID ;
          }
      }
    }

    // Sort results
    var sortedRes = new Vector2Int[res.Count] ;
    int cnt = 0 ;

    foreach (var r in res) {
      sortedRes[cnt] = new Vector2Int (r.Value, r.Key) ;
      cnt ++ ;
    }

    Array.Sort(sortedRes, new Comparison<Vector2Int> (
      (a, b) => estimations[a.y].CompareTo(estimations[b.y])
    )) ;

    return sortedRes ;
  }

}
