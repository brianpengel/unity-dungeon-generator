using UnityEngine ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute(menuName = "Utility Considerations/Can do action")]
public class CanDoActionConsideration : Consideration <DMContext> {

   public enum ActionEnum {
     ATTACK, MOVE, REINFORCE, INVESTIGATE
   }

  [SerializeField] private ActionEnum targetAction ;
  [SerializeField] private bool inverse = false ;

  public override float Evaluate ( DMContext ctx ) {
    var val = Eval ( ctx ) ;
    return inverse ? 1 - val : val ;
  }

  private float Eval ( DMContext ctx ) {
    switch (targetAction) {
      case ActionEnum.ATTACK:
        return (ctx.HasMoved || ctx.HasReinforced || !ctx.CanAttack) ? 0 : 1 ;

      case ActionEnum.REINFORCE:
        return (ctx.HasMoved || ctx.ReinforcementsLeft <= 0 || ctx.HasAttacked) ? 0 : 1 ;

      case ActionEnum.INVESTIGATE:
        return ctx.SituationChanged ? 1 : 0 ;

      default :
        return ctx.CanMove ? 1 : 0 ;
    }
  }

}
