using UnityEngine ;
using ActionEnum = CanDoActionConsideration.ActionEnum ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute(menuName = "Utility Actions/Do turn action")]
public class DoTurnAction : Action<DMContext> {

  [SerializeField] private ActionEnum targetAction ;

  public override void Act ( DMContext ctx ) {
    switch (targetAction) {
      case ActionEnum.ATTACK:
        ctx.DoAttack () ;
        break ;

      case ActionEnum.REINFORCE:
        ctx.DoReinforce () ;
        break ;

      case ActionEnum.INVESTIGATE:
        ctx.DoInvestigate () ;
        break ;

      default :
        ctx.DoMove () ;
        break ;
    }
  }

}
