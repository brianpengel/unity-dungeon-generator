using UnityEngine ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute(menuName = "Utility Considerations/Last Action")]
public class LastActionConsideration : Consideration <ILoggingContext> {

   private enum TargetType {
     EQUALS, IS_NOT
   }

  [SerializeField] private TargetType comparison ;
  [SerializeField] private Action target ;

  public override float Evaluate (ILoggingContext ctx) {
    return (ctx.Log.LastAction == target ^ (comparison == TargetType.IS_NOT)) ? 1 : 0 ;
  }

}
