using UnityEngine ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute(menuName = "Utility Considerations/Risk")]
public class RiskConsideration : Consideration <DMContext> {

   private enum TargetType {
     ENEMY, ALLY
   }

  [SerializeField] private TargetType target ;
  [SerializeField] private float riskCeil = 1 ;
  [SerializeField] private bool inverse = false ;

  public override float Evaluate ( DMContext ctx ) {
    var room = target == TargetType.ENEMY ? ctx.MostRiskyEnemyRoom : ctx.MostRiskyAllyRoom ;
    float risk = room == null ? 0 : ctx.GetRoomRisk(room) ;
    float val =  Mathf.Clamp01(1 / riskCeil * risk) ;

    return inverse ? 1 - val : val ;
  }

}
