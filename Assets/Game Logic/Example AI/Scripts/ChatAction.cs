using UnityEngine ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute(menuName = "Utility Actions/Chat action")]
public class ChatAction : Action<DMContext> {

  [SerializeField] private string text ;

  public override void Act ( DMContext ctx ) {
    ctx.SayMessage (text) ;
  }

}
