﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerController : MonoBehaviour {

	protected Dictionary <int, int[]> rooms ;
	protected RoomManager roomManager ;
	protected System.Action <int, int, int> DoMove ;
	protected System.Action <int, int> DoAttack ;
	protected System.Action <int, int> DoReinforce ;
	private System.Action endTurn ;
	protected bool gameStarted = false ;

	public int ID 			 					{ get; private set ; }
	public int MovesLeft 					{ get;  set ; }
	public int ReinforcementsLeft { get;  set ; }

	public bool HasReinforced			{ get;  set ; }
	public bool HasAttacked				{ get;  set ; }
	public bool HasMoved					{ get { return MovesLeft != 5 ; } }


	public abstract void OnTurnEnd () ;
	public abstract void OnBattled (bool won) ;


	private void Awake () {
		rooms = new Dictionary <int, int[]> () ;
	}

	public void Init (int id, RoomManager roomManager, System.Action <int, int, int> DoMove, System.Action <int, int> DoAttack, System.Action <int, int> DoReinforce, System.Action EndTurn) {
		this.roomManager = roomManager ;
		this.DoReinforce = DoReinforce ;
		this.DoAttack 	 = DoAttack ;
		this.endTurn 		 = EndTurn ;
		this.DoMove 		 = DoMove ;
		this.ID 				 = id ;
	}

	public virtual void OnGameStart () {
		this.gameStarted = true ;
	}

	public virtual void GiveRoom (int i) {
		if(!rooms.ContainsKey(i))
			rooms.Add(i, roomManager.GetAdjacent(i)) ;
	}

	public virtual void TakeRoom (int i) {
		if(rooms.ContainsKey(i))
			rooms.Remove(i) ;
	}

	public virtual void OnTurnStart () {
		var val = 0 ;

		foreach (var r in rooms)
			val += roomManager.GetRoom(r.Key).Value ;

		ReinforcementsLeft = Mathf.Max(2, Mathf.FloorToInt(val * .25f)) ;
		HasReinforced			 = false ;
		HasAttacked				 = false ;
		MovesLeft 				 = 5 ;
	}

	public virtual void OnBattle (bool isAttacker, RoomData origin, RoomData battleField, System.Action<int> roll, System.Action endBattle) {
		HasAttacked = isAttacker ;
	}

	public virtual void OnReinforce (int amount) {
		if(amount == 0) return ;
		ReinforcementsLeft -= amount ;
		HasReinforced = true ;
	}

	public void EndTurn () {
		endTurn () ;
	}

	public virtual void OnMove (int amount) {
		MovesLeft -= amount ;
	}


	protected bool isAlly (RoomData r) {
    return rooms.ContainsKey(r.Room.ID) ;
  }

  protected bool isAlly (int i) {
    return rooms.ContainsKey(i) ;
  }

	public bool hasRooms () {
		return rooms.Count > 0 ;
	}

}
