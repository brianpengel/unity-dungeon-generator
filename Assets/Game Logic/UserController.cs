using System.Collections.Generic ;
using System.Collections ;
using UnityEngine.UI ;
using UnityEngine ;

public class UserController : PlayerController {

  [SerializeField] private Button attackButton, reinforceButton, moveButton, endButton ;
  [SerializeField] private GameObject amountGroup ;
  [SerializeField] private Dropdown dropdown ;
  [SerializeField] private Text amountMessage ;

  private System.Action<int> amountCallback ;
  private Coroutine runningRoutine ;

  public void AttackPhase () {
    if(runningRoutine != null)
      return ;
    SetUIActive (false) ;
    roomManager.RepaintRooms () ;
    runningRoutine = StartCoroutine (BattleRoutine()) ;
  }

  public void MovePhase () {
    if(runningRoutine != null)
      StopCoroutine (runningRoutine) ;
    SetUIActive (false) ;
    moveButton.interactable = false ;
    roomManager.RepaintRooms () ;
    runningRoutine = StartCoroutine (MoveRoutine()) ;
  }

  public void ReinforcePhase () {
    if(runningRoutine != null)
      return ;
    SetUIActive (false) ;
    roomManager.RepaintRooms () ;
    runningRoutine = StartCoroutine (ReinforceRoutine()) ;
  }

  public override void OnMove (int amount) {
    base.OnMove (amount) ;

    if(MovesLeft <= 0)
         EndTurn () ;
    else MovePhase () ;
  }


  public override void OnTurnStart () {
    base.OnTurnStart () ;
    endButton.interactable = true ;
    moveButton.interactable = true ;
    amountGroup.SetActive (false) ;
    SetUIActive (true) ;
  }

	public override void OnTurnEnd () {
    ShowDropdown(false) ;
    if(runningRoutine != null) {
      StopAllCoroutines() ;
      runningRoutine = null ;
    }

    endButton.interactable = false ;
    moveButton.interactable = false ;
    SetUIActive (false) ;
  }

  public override void OnBattle (bool isAttacker, RoomData origin, RoomData battleField, System.Action<int> roll, System.Action endBattle) {
    if(isAttacker)
         SetDropdown("Select how many units to attack with.", 1, Mathf.Min(3, ReinforcementsLeft - 1)) ;
    else SetDropdown("Select how many units to defend with.", 1, Mathf.Min(2, battleField.Occupation)) ;

    ShowDropdown(true) ;

    amountCallback = i=>{
      ShowDropdown(false) ;
      roll (i) ;
      amountCallback = null ;
    };
  }

  public override void OnBattled (bool won) {
    endButton.interactable = true ;
    AttackPhase () ;
  }

  public void SetAmount () {
    if(amountCallback != null) {
      var val = System.Int32.Parse(dropdown.options[dropdown.value].text) ;
      amountCallback (val) ;
    }
  }



  private IEnumerator MoveRoutine () {
    var options = new List <int> () ;
    int clickID = -1, clickID2 = -1, amount = -1 ;
    amountCallback = i => amount = i ;

    // Highlight rooms
    foreach(var room in rooms)
      if(roomManager.GetRoom(room.Key).Occupation > 1)
        foreach (var neighbourID in room.Value)
          if(isAlly(neighbourID)) {
            roomManager.HighlightRoom(room.Key, Color.green) ;
            options.Add (room.Key) ;
            break ;
          }

    yield return MousClickRoutine (id => clickID = id, id => options.IndexOf(id) != -1) ;
    roomManager.RepaintRooms() ;
    options.Clear () ;

    // Highlight targets
    PaintAdjacentEnemies (clickID, Color.green, i => options.Add(i), true) ;

    // wait on selection
    yield return MousClickRoutine (id => clickID2 = id, id => options.IndexOf(id) != -1) ;

    SetDropdown("Select how many units to send.", 1, Mathf.Min(MovesLeft, roomManager.GetRoom(clickID).Occupation - 1)) ;
    ShowDropdown(true) ;

    while (amount == -1)
      yield return null ;
    ShowDropdown (false) ;

    DoMove (clickID, clickID2, amount) ;
  }



  private IEnumerator ReinforceRoutine () {
    int clickID = -1, amount = -1 ;
    amountCallback = i => amount = i ;

    while (ReinforcementsLeft > 0) {
      foreach(var r in rooms)
        roomManager.HighlightRoom(r.Key, Color.green) ;

      yield return MousClickRoutine (id => clickID = id, id => rooms.ContainsKey(id)) ;
      roomManager.RepaintRooms () ;
      roomManager.HighlightRoom(clickID, Color.green) ;

      SetDropdown("Select how many units to send.", 1, ReinforcementsLeft) ;
      ShowDropdown(true) ;

      while (amount == -1)
        yield return null ;

      ShowDropdown (false) ;
      DoReinforce (clickID, amount) ;
      amount = -1 ;
    }

    MovePhase () ;
  }



  private IEnumerator BattleRoutine () {
    var options = new List <int> () ;
    int clickID = -1, clickID2 = -1 ;

    // Highlight zones the player can attack from
    foreach(var room in rooms)
      if(roomManager.GetRoom(room.Key).Occupation > 1)
        foreach(var neighbourID in room.Value)
          if(!rooms.ContainsKey(neighbourID)) {
            roomManager.HighlightRoom(room.Key, Color.green) ;
            options.Add (room.Key) ;
            break ;
          }

    // wait on selection
    yield return MousClickRoutine (id => clickID = id, id => options.IndexOf(id) != -1) ;
    roomManager.RepaintRooms () ;
    options.Clear () ;

    // Highlight targets
    PaintAdjacentEnemies (clickID, Color.green, i => options.Add(i)) ;

    // wait on selection
    yield return MousClickRoutine (id => clickID2 = id, id => options.IndexOf(id) != -1) ;

    roomManager.RepaintRooms () ;
    runningRoutine = null ;
    endButton.interactable = false ;
    DoAttack (clickID, clickID2) ;
  }




  private IEnumerator MousClickRoutine (System.Action<int> callback, System.Func<int, bool> aditionalCheck) {
    int clickID ;

    while (true) {
      yield return null ;
      clickID = -1 ;

      if (Input.GetMouseButtonDown(0) && (clickID = DoMouseClick ()) != -1 && aditionalCheck(clickID)) {
        callback (clickID) ;
        break ;
      }
    }
  }

  private int DoMouseClick () {
    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    RaycastHit hit ;

    if(!Physics.Raycast(ray, out hit) || hit.collider.gameObject.tag != "Tile")
      return -1 ;

    var obj = hit.collider.gameObject.GetComponent<DungeonTileInstance> () ;
    return roomManager.TileToRoomID(obj.index) ;
  }

  private void PaintAdjacentEnemies (int i, Color colour, System.Action<int> entryCallback, bool inverse = false) {
    foreach(var NeighbourID in rooms[i])
      if (!isAlly(NeighbourID) ^ inverse) {
        roomManager.HighlightRoom(NeighbourID, colour) ;
        entryCallback (NeighbourID) ;
      }
  }



  // UI
  private void SetUIActive(bool b) {
		attackButton.interactable = reinforceButton.interactable = b ;
	}

  private void ShowDropdown(bool b) {
		amountGroup.SetActive(b) ;
    moveButton.interactable = !b ;
	}

  private void SetDropdown (string msg, int min, int max) {
    List<string> options = new List<string> () ;

    if(min >= max)
      options.Add(min.ToString()) ;

    else for (int cnt = min; cnt <= max ; cnt ++)
      options.Add(cnt.ToString()) ;

    dropdown.ClearOptions();
    dropdown.AddOptions(options);
    dropdown.value = 0 ;
    amountMessage.text = msg ;
  }


}
