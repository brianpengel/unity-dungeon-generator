﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using RoomType = Room.RoomType ;
using System ;


public class GameController : MonoBehaviour {

	[SerializeField] private PlayerController[] players ;
	[SerializeField] private RoomManager rooms ;
	[SerializeField] private Chat chat ;

	private Comparison<int> descending ;
	private Coroutine runningRoutine ;
	private int turn ;

	void Awake () {
		descending = new Comparison<int>((a, b) => -a.CompareTo(b)) ;
		turn =	players.Length - 1 ;
	}

	private void Start () {
		if(players.Length < 2) return ;
		rooms.DivideRooms (players.Length) ;
		rooms.RepaintRooms () ;
		StartGame () ;
	}

	private void Notify (string msg) {
		chat.SendMessage(msg, Chat.MessageType.INFO) ;
	}

	private void NotifyAction (string msg) {
		chat.SendMessage(msg, Chat.MessageType.ACTION, ExtensionMethods.GetSeededColour(Color.gray, turn + 1)) ;
	}


	private void StartGame () {
		for (int id = 0 ; id < players.Length ; id ++) {
			var player = players[id] ;
			player.Init( id, rooms,
				(o, t, a) => MoveUnits(player.ID, o, t, a),
				(o, t) 		=> StartBattle(player.ID, o, t),
				(t, a)		=> ReinForce(player.ID, t, a),
				AdvanceTurn
			) ;
		}

		foreach (var i in rooms.RoomIDs())
			players[rooms.GetRoom(i).OccupyingPlayer].GiveRoom(i) ;

		for (int id = 0 ; id < players.Length ; id ++)
			players[id].OnGameStart () ;

		AdvanceTurn () ;
	}

	public void AdvanceTurn () {
		rooms.RepaintRooms () ;

		players[turn].OnTurnEnd () ;
		runningRoutine = null ;

		do {
			turn = (turn + 1) % players.Length ;
		} while (!players[turn].hasRooms ()) ;

		Notify("Turn Changed") ;

		if(players[turn] is UserController)
				 NotifyAction("Your turn") ;
		else NotifyAction(String.Format("Player {0} started their turn.", turn + 1)) ;
		players[turn].OnTurnStart () ;
	}

	public int PlayerWon () {
		var prev = -1 ;

		foreach (var i in rooms.RoomIDs()) {
			var curr = rooms.GetRoom(i).OccupyingPlayer ;
			if(prev == -1)
				prev = curr ;

			else if (curr != prev)
				return -1 ;
		}

		return prev ;
	}

	public void EndGame (int winner) {
		NotifyAction (String.Format("Player {0} won.", winner + 1)) ;
	}


	private void MoveUnits (int playerID, int origin, int target, int amount) {
		RoomData og = rooms.GetRoom(origin), t = rooms.GetRoom(target) ;

		if(og != null && t != null && og.OccupyingPlayer == playerID && t.OccupyingPlayer == playerID) {
			Notify( String.Format("{0} unit(s) have been moved from room {1} to {2}.", amount, origin, target) ) ;
			amount = Mathf.Min (amount, players[playerID].MovesLeft) ;
			og.Occupation -= amount ;
			t.Occupation += amount ;
			players[playerID].OnMove (amount) ;
		}
	}

	private void ReinForce (int playerID, int target, int amount) {
		var room = rooms.GetRoom(target) ;

		if(room != null && room.OccupyingPlayer == playerID && players[playerID].ReinforcementsLeft > 0) {
			Notify( String.Format("{0} has been reinforced with {1} unit(s).", target, amount) ) ;
			amount = Mathf.Min (amount, players[playerID].ReinforcementsLeft) ;
			room.Occupation += amount ;
			players[playerID].OnReinforce (amount) ;
		}

		else players[playerID].OnReinforce (0) ;
		rooms.RepaintRooms () ;
	}

	// Battle
	private void StartBattle (int playerID, int origin, int target) {
		if(runningRoutine == null && playerID == turn) {
			var og = rooms.GetRoom(origin) ;
			var t  = rooms.GetRoom(target) ;
			if(og != null && t != null && og.Occupation > 1)
				runningRoutine = StartCoroutine(BattleRoutine(og, t)) ;
		}
	}

	public void EndBattle () {
		if(runningRoutine != null)
			runningRoutine = null ;
	}

	private int[] RollDice (int amount) {
		int[] rolls = new int[amount] ;

		for(int cnt = 0 ; cnt < amount; cnt ++)
			rolls[cnt] = UnityEngine.Random.Range (1, 6) ;

		return rolls ;
	}

	private int AttackerWinAmount (int[] atk, int[] def, out int losses) {
		// Sort Descending
		Array.Sort<int>(atk, descending);
		Array.Sort<int>(def, descending);
		int winCount = 0 ;
		var attempts = Mathf.Min(atk.Length, def.Length) ;

		for(int i = 0 ; i < Mathf.Min(atk.Length, def.Length) ; i ++)
			winCount += atk[i] > def[i] ? 1 : 0 ;

		losses = attempts - winCount ;
		return winCount ;
	}

	private IEnumerator BattleRoutine (RoomData origin, RoomData battleField) {
		NotifyAction( String.Format("Player {0} attacked player {1} at room {2}.", origin.OccupyingPlayer + 1, battleField.OccupyingPlayer + 1, battleField.Room.ID) ) ;

		var deffPlayer = players [battleField.OccupyingPlayer] ;
		var atckPlayer = players [origin.OccupyingPlayer] ;
		int[] attackDice = null, defenseDice = null ;

		System.Action<int> deffDeleg = i => defenseDice = defenseDice == null ? RollDice(Mathf.Clamp(Mathf.Min(2, i), 1, battleField.Occupation)) : defenseDice ,
								  		 atckDeleg = i => attackDice = attackDice == null ? RollDice(Mathf.Clamp(Mathf.Min(3, i), 1, origin.Occupation - 1)) : attackDice ;

		rooms.HighlightRoom (battleField.Room.ID, Color.red) ;
		rooms.HighlightRoom (origin.Room.ID, Color.green) ;

		atckPlayer.OnBattle (true,  origin, battleField, atckDeleg, EndBattle) ;

		// WaitForInput
		while (attackDice == null)
			 yield return null ;
		Notify( String.Format("Attacker rolled {0} dice and threw {1}.", attackDice.Length, ExtensionMethods.JoinArray<int>(attackDice)) ) ;


		yield return new WaitForSeconds(.5f) ;
		deffPlayer.OnBattle (false, origin, battleField, deffDeleg, EndBattle) ;

		// WaitForInput
		while (defenseDice == null)
			yield return null ;
		Notify( String.Format("Defender rolled {0} dice and threw {1}.", defenseDice.Length, ExtensionMethods.JoinArray<int>(defenseDice)) ) ;

		int losses ;
		int fallenUnits = AttackerWinAmount (attackDice, defenseDice, out losses) ;

		battleField.Occupation -= fallenUnits ;
		origin.Occupation -= losses ;

		yield return new WaitForSeconds(.5f) ;
		Notify( String.Format("Lost Units, ATK {0} / {1} DEF.", losses, fallenUnits) ) ;

		// sign over rooms
		if(battleField.Occupation < 1) {
			yield return new WaitForSeconds(.5f) ;
			battleField.OccupyingPlayer = origin.OccupyingPlayer ;
			battleField.Occupation = origin.Occupation - 1 ;
			origin.Occupation = 1 ;

			deffPlayer.TakeRoom (battleField.Room.ID) ;
			atckPlayer.GiveRoom (battleField.Room.ID) ;
		}

		rooms.RepaintRooms () ;
		runningRoutine = null ;

		var j = PlayerWon () ;
		if(j == -1)
				 atckPlayer.OnBattled ( battleField.Occupation < 1) ;
		else EndGame (j) ;
	}


}
