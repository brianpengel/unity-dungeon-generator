using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using RoomType = Room.RoomType ;


public class RoomManager : MonoBehaviour {

  [SerializeField] private DungeonGridGenerator generator ;
  [SerializeField] private DungeonGridRenderer renderer ;
  [SerializeField] private DungeonGrid grid ;
  [SerializeField] private List <Path> paths ;
  private Dictionary <int, RoomData> rooms ;

  private void Awake () {
		generator.Generate(grid) ;
		renderer.Render(grid) ;

		rooms = GetRooms () ;
		paths = CalculatePaths () ;
	}


  public int CheckForDomination () {
		var control = rooms[0].OccupyingPlayer ;

		for (int i = 1 ; i < rooms.Count ; ++ i)
			if(rooms[i].OccupyingPlayer != control)
				return -1 ;
		return control ;
	}


  public void DivideRooms (int playerCount) {
		var initialScores = new int [playerCount] ;

		foreach (var room in rooms) {
			var smallest = 0 ;

			for(int i = 1 ; i < initialScores.Length ; ++ i)
				if(initialScores[smallest] > initialScores[i])
				 	smallest = i ;

			initialScores[ smallest ] += room.Value.Value ;
			room.Value.OccupyingPlayer = smallest ;
			room.Value.Occupation = 10 ;
		}
	}

  public void RepaintRooms () {
    foreach (var room in rooms) {
      renderer.SetRoomColour(room.Key, ExtensionMethods.GetSeededColour(Color.gray, room.Value.OccupyingPlayer + 1)) ;
      // DEBUG
      // renderer.SetRoomText (room.Value.Room.ID, room.Value.Room.ID + " | " +  RankRoom(room.Value).ToString("0.00") + " | " + room.Value.Occupation.ToString()) ;
      renderer.SetRoomText (room.Value.Room.ID, room.Value.Occupation.ToString()) ;
    }
  }

  public void HighlightRoom (int id, Color colour = default(Color)) {
    if(rooms.ContainsKey(id)) {
      renderer.SetRoomColour(id, colour) ;
      // DEBUG
      // renderer.SetRoomText (id, rooms[id].Room.ID + " | " +  RankRoom(rooms[id]).ToString("0.00") + " | " + rooms[id].Occupation.ToString()) ;
      renderer.SetRoomText (id, rooms[id].Occupation.ToString()) ;
    }
  }

  // DEBUG
  private float RankRoom (RoomData room) {
    return RankRoom(room.Room.ID, room.Occupation, room.OccupyingPlayer) ;
  }

  private float RankRoom (int i, int occupation, int playerID) {
    float risk = 0, enemies = 0;

    foreach (var neighbourID in GetAdjacent(i))
      if(rooms.ContainsKey(neighbourID)) {
        var neighbour = rooms[neighbourID] ;

        if(neighbour.OccupyingPlayer != playerID) {
          int diff = neighbour.Occupation - occupation ;
          risk += (diff + 1) * .1f ;
          enemies ++ ;
        }
      }

    return enemies == 0 ? -Mathf.Infinity : risk ;
  }
  // DEBUG



  // Room translation
  public RoomData GetRoom (int id) {
		return rooms.ContainsKey(id) ? rooms [id] : null ;
	}

  public int TileToRoomID (int id) {
		return grid.GetUsedTile(id) ;
	}

	private Dictionary <int, RoomData> GetRooms () {
		var rooms = new Dictionary <int, RoomData> () ;

		foreach (var room in grid.RoomsOfType(RoomType.DEFAULT))
			rooms.Add (room.ID, new RoomData( room )) ;

		return rooms ;
	}

	public int[] GetAdjacent (int id) {
		if(!rooms.ContainsKey(id)) return null ;

		var res = new List<int> () ;

		// foreach connector
		foreach (var c in rooms[id].Connectors)
			// Check if there is a path that starts with the connector
			foreach (var p in paths) {
				if(p.Start == c.Value)
				// Search for room at the end
					foreach (var room in rooms)
						if(room.Key != id && room.Value.Connectors.ContainsValue(p.End)) {
							res.Add(room.Key) ;
							break ;
						}
				}

		return res.ToArray() ;
	}

	// Path finding
	private List <Path> CalculatePaths () {
		// var searchedNodes = new List <int> () ;
		var dormantNodes 	= new Stack <PathFindingNode> () ;
		var paths 				= new List <Path> () ;
		PathFindingNode current ;

		// Add all connectors to be searched
		foreach (var connector in grid.RoomsOfType(RoomType.CONNECTOR))
			dormantNodes.Push(new PathFindingNode(null, connector.Position)) ;

		// Search for paths
		while (dormantNodes.Count > 0) {
			current = dormantNodes.Pop () ;
			var room = grid.GetRoom(grid.GetUsedTile(current.position)) ;

			// If the current node is the end of a path
			if(room.Type == RoomType.CONNECTOR && current.prev != null)
				paths.Add((Path) current) ;

			// Get Neighbours
			else {
				var neighbours = grid.TileDirectNeighbours (current.position) ;
				int roomCount = 0 ;

				foreach (var n in neighbours)
					if(grid.isTileUsed(n) && (current.prev == null || n != current.prev.position) && !current.Contains(n)){
						var nRoom = grid.GetRoom(grid.GetUsedTile(n)) ;
						if(nRoom.Type != RoomType.DEFAULT)
							dormantNodes.Push(new PathFindingNode(current, n)) ;

						else if(room.Type == RoomType.CONNECTOR) {
							// Add connector to rooms dictionary of connectors
							if(!rooms[nRoom.ID].Connectors.ContainsKey(n))
								rooms[nRoom.ID].Connectors.Add (n, room.Position) ;
							roomCount ++ ;
						}
					}

				if(roomCount > 1)
					paths.Add((Path) current) ;
			}
		}

		return paths ;
	}

  public IEnumerable<int> RoomIDs () {
    foreach (var i in rooms)
      yield return i.Key ;
  }

	private void DebugCross (Vector2Int pos, int w, int h, Color colour, int duration) {
		var dPos = DungeonGridRenderer.POSITION_ROTATION  * (Vector3)(Vector2)pos ;
		Debug.DrawRay(dPos - Vector3.forward * .5f, Vector3.forward * h, colour, duration) ;
		Debug.DrawRay(dPos - Vector3.right * .5f, 	Vector3.right * w, colour, duration) ;
	}




	[System.Serializable]
	public class Path {
		public int Start, End ;
		public int[] tiles ;

		public Path Inverse () {
			var res = new Path () ;
			res.tiles = tiles.Clone () as int[] ;
			System.Array.Reverse(res.tiles);
			res.Start = res.tiles [0] ;
			res.End = res.tiles [res.tiles.Length - 1] ;
			return res ;
		}

		public static explicit operator int[] (Path node) {
			return node.tiles ;
		}
	}

	private class PathFindingNode {

		public PathFindingNode origin { get { return prev == null ? this : prev.origin ; } }
		public PathFindingNode prev 	{ get ; private set ; }
		public int position		  			{ get ; private set ; }
		public int length 						{ get ; private set ; }

		public PathFindingNode (PathFindingNode prev, int position) {
			this.position	= position ;
			this.length   = prev == null ? 1 : prev.length + 1 ;
			this.prev 	  = prev ;
		}

		public bool Contains (int i) {
			return position == i || !(prev == null || !prev.Contains(i)) ;
		}

		public static explicit operator Path (PathFindingNode node) {
			var res = new Path () ;
			res.tiles = (int[])node ;
			res.Start = res.tiles [0] ;
			res.End = res.tiles[res.tiles.Length - 1] ;
			return res ;
		}

		public static explicit operator int[] (PathFindingNode node) {
			var res = new int[node.length] ;

			var cur = node ;
			do {
				res[cur.length - 1] = cur.position ;
				cur = cur.prev ;
			} while (cur != null) ;

			return res ;
		}
	}
}
