using UnityEngine ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute(menuName = "Utility Actions/Random action")]
public class RandomActionDecorator : DecoratorAction {

  public override void Act ( IContext ctx ) {
    if(actions.Count > 0)
      actions[ Random.Range(0, actions.Count) ].Act (ctx) ;
  }

}
