using System.Collections.Generic ;
using UnityEngine ;

/*
 *
 *
 */

[System.Serializable]
public abstract class DecoratorAction : Action {

  [SerializeField] protected List<Action> actions ;

}
