using UnityEngine ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute(menuName = "Utility Actions/Action sequence")]
public class SequenceActionDecorator : DecoratorAction {

  public override void Act ( IContext ctx ) {
    foreach (var a in actions)
      a.Act (ctx) ;
  }

}
