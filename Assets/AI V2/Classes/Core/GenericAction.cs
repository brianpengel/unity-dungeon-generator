
/*
 *
 *
 */

[System.Serializable]
public abstract class Action<TContext> : Action where TContext : class, IContext {

  public abstract void Act ( TContext context ) ;

  public sealed override void Act ( IContext context ) {
    Act(context as TContext) ;
  }

}
