using UnityEngine ;

/*
 *
 *
 */

[System.Serializable]
public abstract class Consideration : ScriptableObject, IDescribable {

  [SerializeField] private string description = "[Description]" ;
  [SerializeField] private AnimationCurve evalCurve ;


  public string Name {
    set { name = value ; }
    get { return name  ; }
  }

  public string Description {
    set { description = value ; }
    get { return description  ; }
  }


  public abstract float Evaluate ( IContext context ) ;

  public float ApplyResponseCurve (float score) {
    return evalCurve.Evaluate (score) ;
  }

}
