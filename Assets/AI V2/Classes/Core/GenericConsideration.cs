
/*
 *
 *
 */

[System.Serializable]
public abstract class Consideration <TContext> : Consideration where TContext : class, IContext {

  public abstract float Evaluate ( TContext context ) ;

  public sealed override float Evaluate ( IContext context ) {
    return Evaluate (context as TContext) ;
  }

}
