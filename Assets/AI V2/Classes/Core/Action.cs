using System.Collections.Generic ;
using UnityEngine ;

/*
 *
 *
 */

[System.Serializable]
public abstract class Action : ScriptableObject, IDescribable {

  [SerializeField] private string description = "[Description]" ;


  public string Name {
    set { name = value ; }
    get { return name  ; }
  }

  public string Description {
    set { description = value ; }
    get { return description  ; }
  }


  public abstract void Act ( IContext context ) ;

}
