using System.Collections.Generic ;
using UnityEngine ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute]
public class Intelligence : ScriptableObject, IDescribable {

  [SerializeField] private string description = "[Description]" ;
  [SerializeField] private float startScoreMinimum = 0 ;
  [SerializeField] private List<ActionSet> sets ;


  public string Name {
    set { name = value ; }
    get { return name  ; }
  }

  public string Description {
    set { description = value ; }
    get { return description  ; }
  }


  public Action Act <TContext> (TContext context) where TContext : class, IContext {
    PotentialAction viableAction = null ;
    float floor = startScoreMinimum ;

    foreach (var set in sets) {
      var action = set.EvaluateActions <TContext> (context, ref floor) ;

      if(action != null)
        viableAction = action ;
    }

    if(viableAction == null)
      return null ;

    viableAction.Execute <TContext> ( context ) ;
    return viableAction.Action ;
  }
}
