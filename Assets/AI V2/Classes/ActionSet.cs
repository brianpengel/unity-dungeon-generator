using System.Collections.Generic ;
using UnityEngine ;

/*
 *
 *
 */

[System.Serializable, CreateAssetMenuAttribute]
public class ActionSet : ScriptableObject, IDescribable {

  [SerializeField] private string description = "[Description]" ;
  [SerializeField] private List<PotentialAction> actions ;


  public string Name {
    set { name = value ; }
    get { return name  ; }
  }

  public string Description {
    set { description = value ; }
    get { return description  ; }
  }



  public PotentialAction EvaluateActions <TContext> (TContext context, ref float floor) where TContext : class, IContext {
    PotentialAction mostViable = null ;

    foreach(var action in actions) {
      float score = action.Evaluate <TContext> (context, floor) ;

      if(score > floor) {
        mostViable = action ;
        floor = score ;
      }
    }

    return mostViable ;
  }

}
