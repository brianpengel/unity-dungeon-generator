using UnityEngine ;

[System.Serializable]
public class PotentialAction {

  [SerializeField] private Evaluator evaluator ;
  [SerializeField] private Action action ;

  public Evaluator Evaluator { get { return evaluator ; } }
  public Action Action       { get { return action ; } }

  public PotentialAction (Evaluator evaluator, Action action) {
    this.evaluator = evaluator ;
    this.action    = action ;
  }

  public float Evaluate <TContext> (TContext context, float floor) where TContext : class, IContext  {
    return evaluator.Evaluate <TContext> (context, floor) ;
  }

  public void Execute <TContext> (TContext context) where TContext : class, IContext  {
    action.Act (context) ;
  }

}
