using System.Collections.Generic ;
using UnityEngine ;

/*
 * bonus = Weight + momentum + other bonuses
 * momentum = How likely am i to change my mind
 */

[System.Serializable, CreateAssetMenuAttribute]
public class Evaluator : ScriptableObject, IDescribable {

  [SerializeField] private string description = "[Description]" ;
  [SerializeField] private float  weight      = 1 ;

  [SerializeField] private List<Consideration> considerations ;



  public string Name {
    set { name = value ; }
    get { return name  ; }
  }

  public string Description {
    set { description = value ; }
    get { return description  ; }
  }

  public float  Weight {
    set { Weight = value ; }
    get { return Weight  ; }
  }



  private float ApplyCompensation (float val) {
    float modificationFactor = 1f - ( 1f / considerations.Count ) ;
    float makeUpValue        = ( 1f - val ) * modificationFactor ;
    return val + ( makeUpValue * val ) ;
  }


  public float Evaluate <TContext> (TContext context, float min, float bonus = 1) where TContext : class, IContext {
    float evaluation = bonus ;

    foreach(var consideration in considerations) {
      if(evaluation <= 0 || evaluation < min)
        return 0 ;

      float score = consideration.Evaluate ( context ) ;
      // Debug.Log(score) ;
      float response = consideration.ApplyResponseCurve( score ) ;

      evaluation *= response ;
    }

    return weight * Mathf.Clamp01 ( ApplyCompensation ( evaluation ) ) ;
  }

}
