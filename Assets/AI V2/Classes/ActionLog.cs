using System.Collections.Generic ;
using UnityEngine ;

/*
 * TODO improve log,
 *
 */

public class ActionLog {

  private Dictionary <Action, float> actionLog ;
  private Queue <Action> logOrder ;
  private int logLength ;

  public Action LastAction { get ; private set ; }

  public ActionLog (int length) {
    actionLog = new Dictionary <Action, float> ( length ) ;
    logOrder = new Queue <Action> ( length ) ;
    logLength = length ;
    LastAction = null ;
  }

  public float LastExecutionOfAction (Action action) {
    if(actionLog.ContainsKey (action))
      return actionLog [action] ;
    return -1 ;
  }

  public void LogAction (Action action) {
    if(logOrder.Count >= logLength) {
      var first = logOrder.Dequeue () ;
      if(logOrder.Count < actionLog.Count)
        actionLog.Remove (first) ;
    }

    logOrder.Enqueue (action) ;
    LastAction = action ;

    if(actionLog.ContainsKey (action))
         actionLog [action] = Time.time ;
    else actionLog.Add (action, Time.time) ;
  }
}
