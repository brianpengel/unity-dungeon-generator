public interface ILoggingContext : IContext {

  ActionLog Log { get ; }

}
