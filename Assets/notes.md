- Actions
- Axes
  - Score

##### Vereisten
- Normalized Input ( 0 - 1 )
- Infinite Axes
- Compensation factor (
    ModificationFactor = 1 - ( 1/NumberOfAxes )
    MakeUpValue        = ( 1-Score ) * ModificationFactor
    CompensatedScore   = Score + ( MakeUpValue*Score )
  )
- Actions with targets are scores per target
- Reference other Decisions / Actions

##### Inputs
- Data Objects ( From Characters/Game Engine )
- Decision Context
  - Decision identifier
    "What are you trying to do?"
  - Link to intelligence controller
    "Who is asking?"
  - Link to content data with parameters
    "What do you need?"
  - Optional link to context object
    "Who is it happening to?"

##### Axes / Considerations
- Name / Description
- input
- Response Curve
-


##### TODO
- AI
- Renderer
- Combat



##### AI
RISK
- AI
  - Attack
  - Reorganize

- PAWN AI
  - Emotes
<!-- - Dungeon Master
  - Lock player in room with doors.
  - Spawn Pawns
  - Inspect rooms
  - Manage Pawns

- Pawn
  - Navigate
  - Attack player
  - Die -->
