using System.Collections.Generic ;
﻿using System.Collections ;
using System.Linq;

using UnityEngine.UI ;
using UnityEngine ;


public class Chat : MonoBehaviour {

	private static readonly Color ACTION_COLOUR ;
	private static readonly Color INFO_COLOUR ;

	public enum MessageType { ACTION, INFO }

	[SerializeField] private int maxMessageCount = 10 ;
	[SerializeField] private RectTransform chatObj ;
	[SerializeField] private RectTransform msgPrefab ;
	private Queue <Message> messages ;


	static Chat () {
		ACTION_COLOUR = new Color (.224f, .56f, .85f) ;
		INFO_COLOUR 	= new Color (.396f, .396f, .396f) ;
	}

	void Awake () {
		FillChat () ;
		MoveMessages () ;
	}

	public void SendMessage (string identifier, string msg, MessageType type = MessageType.INFO, Color? colour = null) {
		var fullMessage = System.String.Join(" ", new []{"[", identifier, "]", msg}) ;
		SendMessage (fullMessage, type, colour) ;
	}

	public void SendMessage (string msg, MessageType type = MessageType.INFO, Color? colour = null) {
		var message = messages.Dequeue () ;
		message.text.text = msg ;
		StyleMessage (message.text, type, colour) ;
		message.transform.gameObject.SetActive (true) ;

		messages.Enqueue (message) ;
		MoveMessages () ;
	}

	private void StyleMessage (Text textComp, MessageType type, Color? overwrittenColour = null) {
		switch (type) {
			case MessageType.ACTION :
				textComp.color = ACTION_COLOUR ;
				textComp.fontStyle = FontStyle.BoldAndItalic ;
				break ;

			default :
				textComp.color = INFO_COLOUR ;
				textComp.fontStyle = FontStyle.Italic ;
				break ;
		}

		if(overwrittenColour != null)
			textComp.color = (Color)overwrittenColour ;
	}

	private void MoveMessages () {
		float offset = 0 ;

		foreach (var msg in messages.Reverse()) {
			var delta = msg.transform.sizeDelta ;
			msg.transform.offsetMin = new Vector2 (0, offset);
			msg.transform.sizeDelta = delta ;
			offset += delta.y ;
		}
	}

	private void FillChat () {
		messages = new Queue <Message> (maxMessageCount) ;

		for(int cnt = 0 ; cnt < maxMessageCount ; cnt ++) {
			var transform = Instantiate(msgPrefab) ;
			var msg = new Message (
				transform,
				transform.GetComponentsInChildren<Text> () [0]
			);

			transform.SetParent (chatObj, false) ;
			transform.gameObject.SetActive (false) ;
			messages.Enqueue (msg) ;
		}
	}



	private struct Message {
		public RectTransform transform ;
		public Text text ;

		public Message (RectTransform transform, Text text) {
			this.transform = transform ;
			this.text = text ;
		}
	}
}
