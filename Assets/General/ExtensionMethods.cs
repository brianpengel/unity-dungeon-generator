using System.Collections.Generic ;
using UnityEngine ;
using System ;

public static class ExtensionMethods {

  // public static EDirection Inverse (this EDirection dir) {
  //   var a = Mathf.Log((int)dir, 2) ;
	// 	return (EDirection)Mathf.Pow(2, (7 - a)) ;
	// }

  public static float Map(this float x, float in_min, float in_max, float out_min, float out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  }

  public static int OddRange(int min, int max) {
    var m = max - min + 2 ;
    return min + ((int) UnityEngine.Random.Range(0, m * .5f)) * 2 ;
  }

  public static int EvenRange(int min, int max) {
    return OddRange(min, max) + 1 ;
  }

  public static void Fill<T>(this T[] originalArray, T with) {
    for(int i = 0; i < originalArray.Length; i++)
      originalArray[i] = with;
  }

  public static void Fill<T>(this T[] originalArray) where T : new() {
    for(int i = 0; i < originalArray.Length; i++)
      originalArray[i] = new T ();
  }

  // public static UInt32 Count(this EDirection e) {
  //     UInt32 v = (UInt32)e;
  //     v = v - ((v >> 1) & 0x55555555); // reuse input as temporary
  //     v = (v & 0x33333333) + ((v >> 2) & 0x33333333); // temp
  //     UInt32 c = ((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24; // count
  //     return c;
  // }

  // Knuth shuffle algorithm
  public static T[] Shuffle <T> (T[] indices) {
    for (int t = 0; t < indices.Length; t++ ) {
      T tmp = indices[t];
      int r = UnityEngine.Random.Range(t, indices.Length);
      indices[t] = indices[r];
      indices[r] = tmp;
    }

    return indices ;
  }

  public static List<T> Shuffle <T> (List<T> indices) {
    for (int t = 0; t < indices.Count; t++ ) {
      T tmp = indices[t];
      int r = UnityEngine.Random.Range(t, indices.Count);
      indices[t] = indices[r];
      indices[r] = tmp;
    }

    return indices ;
  }

  public static Color GetSeededColour (Color mix, int seed) {
    UnityEngine.Random.State originalSeed = UnityEngine.Random.state ;
    UnityEngine.Random.InitState(seed) ;

    float green = UnityEngine.Random.Range(0f, 1f) ;
    float blue  = UnityEngine.Random.Range(0f, 1f) ;
    float red   = UnityEngine.Random.Range(0f, 1f) ;

    green = (green  + mix.g) * .5f;
    blue  = (blue   + mix.b) * .5f;
    red   = (red    + mix.r) * .5f;

    UnityEngine.Random.state = (originalSeed) ;

    Color color = new Color(red, green, blue);
    return color;
  }

  public static string JoinArray <T> (T[] arr) {
    var builder = new System.Text.StringBuilder();
    Array.ForEach(arr, x => builder.Append(x + ", "));
    return builder.ToString();
  }

}
